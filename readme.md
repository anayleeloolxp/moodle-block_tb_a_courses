# Leeloo LXP Available Courses Block
This moodle block shows all the available courses for a user. 
Available means the courses user is not enrolled.

Installation Instructions
=========================

* Make sure you have all the required versions.
* Download and unpack the block folder.
* Place the folder (eg "tb_a_courses") in the "blocks" subdirectory.
* Visit http://yoursite.com/admin to complete the installation
* Turn editing on the my page.
* Add the block to the page ("Leeloo LXP Available Courses Block (tb_a_courses)")
* Visit the config link in the block for more options.

Moodle compatibility
=====================
* Tested with Moodle 3.1, 3.2, 3.3 and 3.4

License
=====================

GPL 3 or later